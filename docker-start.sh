#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

function install_fun(){
echo "this system has not installed docker"
echo "starting install docker and docker-compose"
# install docker
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun

# set aliyun image accelerator
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://cj9kwv26.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker

# docker install check
docker version


# install docker compse
sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# docker compose install check
docker-compose --version
}


docker=$(rpm -qa | grep "docker-ce")

if [ "$docker" == "" ]; then
    install_fun;
else
    echo "this system has installed docker"
fi

# start docker service
sudo systemctl start docker.service

# set boot automatically start docker service
sudo systemctl enable docker.service

echo "building images and start"
docker-compose up
docker-compose stop
docker-compose start
