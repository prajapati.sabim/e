#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset


mv /postgresql.conf /var/lib/postgresql/data/postgresql.conf