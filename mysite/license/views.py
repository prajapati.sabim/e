import os
import json
import mimetypes
import platform
from xml.dom import minidom
from xml.etree import ElementTree
from wsgiref.util import FileWrapper

import requests
from django.conf import settings
from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

from mysite.license.models import License
from mysite.license.forms import OnlineActiveForm
from mysite.license.zk_license import ZKLicense
from mysite.license.utils import date_convert
from mysite.utils.rsa_key import encryption, decryption

MEDIA_ROOT = settings.MEDIA_ROOT
ACTIVITY_URL = "http://license.zkteco.com/authController.do?fileOnlineActiveAuth&xmlInfo="  # noqa E501


@login_required
@permission_required('license.browse_license', raise_exception=True)
def license_home(request):
    license_flag = False  # 数据库中是否有license标识符
    is_license_error = False  # 数据库中license是否被修改标识符
    license = License.objects.all()
    # 从数据库中获取许可信息
    if license.count() > 0 and \
            license[0].license_number_device and \
            license[0].license_number_employee:
        license = license[0]
        try:
            employee_limit = decryption(license.employee_limit)
            device_limit = decryption(license.device_limit)
            expired_data = decryption(license.expired_data)
            license_flag = True

            return render(request, 'license/license_home.html', {
                'license': license,
                'device_limit': device_limit,
                'employee_limit': employee_limit,
                'expired_data': expired_data,
                'license_flag': license_flag,
                'is_license_error': is_license_error
            })
        except Exception:
            is_license_error = True  # 数据库中license数据被修改
            return render(request, 'license/license_home.html', {
                'license_flag': license_flag,
                'is_license_error': is_license_error
            })
    # 从注册表中获取许可信息
    elif platform.system() == 'Windows':
        from mysite.license.regedit import LicenseRegedit
        if LicenseRegedit.check_regedit():
            try:
                employee_limit = decryption(
                    LicenseRegedit.get_regedit('TrialEmployee'))
                device_limit = decryption(
                    LicenseRegedit.get_regedit('TrialDevice'))
                expired_data = decryption(
                    LicenseRegedit.get_regedit('TrialDate'))
                license_flag = True

                return render(request, 'license/license_home.html', {
                    'license': license,
                    'device_limit': device_limit,
                    'employee_limit': employee_limit,
                    'expired_data': expired_data,
                    'license_flag': license_flag,
                    'is_license_error': is_license_error
                })
            except Exception:
                is_license_error = True  # 数据库中license数据被修改
                return render(request, 'license/license_home.html', {
                    'license_flag': license_flag,
                    'is_license_error': is_license_error
                })
        else:
            return render(request, 'license/license_home.html', {
                'license_flag': license_flag,
                'is_license_error': is_license_error
            })
    else:
        return render(request, 'license/license_home.html', {
            'license_flag': license_flag,
            'is_license_error': is_license_error
        })


@login_required
@permission_required('license.add_license', raise_exception=True)
def create_upk_file(request):
    if request.method != 'POST':
        return render(request, 'license/create_upk.html',
                      {'form': OnlineActiveForm()})
    else:
        form = OnlineActiveForm(request.POST, request.FILES)
        if not form.is_valid():
            return render(request, 'license/create_upk.html',
                          {'form': OnlineActiveForm()})
        else:
            sn_file = request.FILES.get('sn_file')
            file_type = os.path.splitext(sn_file.name)[1]
            if file_type != '.xml':
                back = {"message": "SN file type is Error!",
                        "type": "upk", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            size_kb = sn_file.size // 1024
            if size_kb > 500:
                back = {"message": "SN file size is too big!",
                        "type": "upk", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            try:
                content = ElementTree.parse(sn_file)
            except Exception:
                back = {"message": "SN file content is Error!",
                        "type": "upk", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            id_element = content.getiterator('id')
            ids = [i.text for i in id_element]
            first_id = ids[0].strip()
            type_id_1 = ids[1]
            type_id_2 = ids[2]

            sn_element = content.getiterator('serialNumber')
            serial_number_1 = sn_element[0].text
            serial_number_2 = sn_element[1].text

            if int(type_id_1) != 30 or int(type_id_2) != 35 or not first_id:
                back = {"message": "SN Type Error!",
                        "type": "upk", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            if not serial_number_1 or not serial_number_2:
                back = {
                    "message": "Your sn file is wrong, Serial number can not be NULL!",  # noqa E501
                    "type": "upk", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            split_sn_1 = serial_number_1.replace("-", "")
            split_sn_2 = serial_number_2.replace("-", "")
            zk_license = ZKLicense()
            upk_1 = zk_license.get_upk(split_sn_1)
            upk_2 = zk_license.get_upk(split_sn_2)

            if not upk_1 or not upk_2:
                back = {"message": 'Your sn file is Error!',
                        "type": "upk", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})
            else:
                doc = minidom.Document()
                infoxml = doc.createElement('xmlinfo')
                doc.appendChild(infoxml)
                # 一级节点
                id_first = doc.createElement('id')
                company_xml = doc.createElement('company')
                license_xml = doc.createElement('license')
                # 添加一级节点到根节点
                infoxml.appendChild(id_first)
                infoxml.appendChild(company_xml)
                infoxml.appendChild(license_xml)
                # 一级节点"id"内容的添加
                id_first_text = doc.createTextNode(str(first_id))
                id_first.appendChild(id_first_text)
                # 二级节点
                xml_module_1 = doc.createElement('module')
                xml_module_2 = doc.createElement('module')
                xml_country = doc.createElement('country')
                xml_city = doc.createElement('city')
                xml_name = doc.createElement('name')
                xml_email = doc.createElement('email')
                xml_contacts = doc.createElement('contacts')
                xml_address = doc.createElement('address')
                xml_industry = doc.createElement('industry')
                # 添加二级节点到一级节点
                license_xml.appendChild(xml_module_1)
                license_xml.appendChild(xml_module_2)
                company_xml.appendChild(xml_country)
                company_xml.appendChild(xml_city)
                company_xml.appendChild(xml_name)
                company_xml.appendChild(xml_email)
                company_xml.appendChild(xml_contacts)
                company_xml.appendChild(xml_address)
                company_xml.appendChild(xml_industry)
                # 二级节点内容的添加

                xml_name.appendChild(doc.createTextNode(
                    form.cleaned_data.get('company_name')))
                xml_country.appendChild(doc.createTextNode(
                    form.cleaned_data.get('country')))
                xml_city.appendChild(doc.createTextNode(
                    form.cleaned_data.get('city')))
                xml_email.appendChild(doc.createTextNode(
                    form.cleaned_data.get('email_address')))
                xml_address.appendChild(doc.createTextNode(form.cleaned_data.get(  # noqa 503
                    'address') if form.cleaned_data.get('address') else ''))
                xml_industry.appendChild(doc.createTextNode(form.cleaned_data.get(  # noqa 503
                    'industry') if form.cleaned_data.get('industry') else ''))
                xml_contacts.appendChild(doc.createTextNode(form.cleaned_data.get(  # noqa 503
                    'contacts') if form.cleaned_data.get('contacts') else ''))

                # 三级节点
                id_second_1 = doc.createElement('id')
                serialNumber_1 = doc.createElement('serialNumber')
                upk_xml_1 = doc.createElement('upk')
                # 添加三级节点到二级节点"module"
                xml_module_1.appendChild(id_second_1)
                xml_module_1.appendChild(serialNumber_1)
                xml_module_1.appendChild(upk_xml_1)
                # 三级节点"id","upk"和"serialNumber"和内容的添加
                id_second_text = doc.createTextNode(type_id_1)
                id_second_1.appendChild(id_second_text)
                serialNumber_text_1 = doc.createTextNode(serial_number_1)
                serialNumber_1.appendChild(serialNumber_text_1)
                upk_text_1 = doc.createTextNode(upk_1)
                upk_xml_1.appendChild(upk_text_1)

                # 三级节点
                id_second_2 = doc.createElement('id')
                serialNumber_2 = doc.createElement('serialNumber')
                upk_xml_2 = doc.createElement('upk')
                # 添加三级节点到二级节点"module"
                xml_module_2.appendChild(id_second_2)
                xml_module_2.appendChild(serialNumber_2)
                xml_module_2.appendChild(upk_xml_2)
                # 三级节点"id","upk"和"serialNumber"和内容的添加
                id_second_text = doc.createTextNode(type_id_2)
                id_second_2.appendChild(id_second_text)
                serialNumber_text_2 = doc.createTextNode(serial_number_2)
                serialNumber_2.appendChild(serialNumber_text_2)
                upk_text_2 = doc.createTextNode(upk_2)
                upk_xml_2.appendChild(upk_text_2)

                file_path = "%s/%s" % (MEDIA_ROOT, 'upk')
                if not os.path.exists(file_path):
                    os.makedirs(file_path)
                upk_name = "%s%s%s" % ('upk-', str(first_id), '.xml')
                upk_path = "%s/%s/%s" % (MEDIA_ROOT, 'upk', upk_name)

                if os.path.exists(upk_path):
                    os.remove(upk_path)

                with open(upk_path, 'w+') as f:
                    doc.writexml(f)

                back = {"message": 'Create UPK successful',
                        "type": "upk", "flag": True}

    return render(request, 'license/license_return.html', {'back': back})


@login_required
@permission_required('license.change_license', raise_exception=True)
def download_upk_file(request):
    path = settings.MEDIA_ROOT + '/upk'

    dir_list = os.listdir(path)
    for i in dir_list:
        if os.path.isfile(os.path.join(path, i)):
            file_path = os.path.join(path, i)
    # file_path = os.path.join(path, dir_list[0])
    file_name = i
    chunk_size = 512

    response = StreamingHttpResponse(
        FileWrapper(open(file_path, 'rb'), chunk_size),
        content_type=mimetypes.guess_type(file_path)[0]
    )
    response['Content-Type'] = 'application/octet-stream'
    response['Content-Length'] = os.path.getsize(file_path)
    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(file_name)  # noqa E501

    return response


@login_required
@permission_required('license.add_license', raise_exception=True)
def active_offline(request):

    if request.method != 'POST':
        return render(request, 'license/license_offline.html')
    else:
        license_file = request.FILES.get('license_file')
        if not license_file:
            return render(request, 'license/license_offline.html')
        else:
            try:
                content = minidom.parse(license_file)

                city = content.getElementsByTagName('city')[0].firstChild.data
                name = content.getElementsByTagName('name')[0].firstChild.data
                country = content.getElementsByTagName(
                    'country')[0].firstChild.data
                email = content.getElementsByTagName(
                    'email')[0].firstChild.data
                contacts = content.getElementsByTagName(
                    'contacts')[0].firstChild.data
                address = content.getElementsByTagName(
                    'address')[0].firstChild.data
                industry = content.getElementsByTagName(
                    'industry')[0].firstChild.data
                type_id_device = content.getElementsByTagName('id')[1].firstChild.data  # noqa E501
                license_number_device = content.getElementsByTagName(
                    'licenseNumber')[0].firstChild.data
                type_id_employee = content.getElementsByTagName('id')[2].firstChild.data  # noqa E501
                license_number_employee = content.getElementsByTagName(
                    'licenseNumber')[1].firstChild.data
            except Exception as e:
                print(e)
                back = {"message": "License file is Error!",
                        "type": "offline", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            if int(type_id_device) != 30 or int(type_id_employee) != 35:
                back = {"message": "License file Type Error!",
                        "type": "offline", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            license_counts = License.objects.all().count()
            if license_counts > 0:
                License.objects.all().delete()  # 删除数据库中已经被修改的license

            license = License()
            license.country = country
            license.city = city
            license.email_address = email
            license.contacts = contacts
            license.company_name = name
            license.address = address
            license.industry = industry
            license.license_file = license_file

            zk_license = ZKLicense()
            # 解析设备device的许可
            result_device = zk_license.get_license_info(
                license_number_device.replace("-", ""))

            if result_device["major"] == 1:
                license.pro = 1
            license.license_number_device = license_number_device

            int_device_number = int(result_device['limit_devices'])
            if int(result_device['minor']) != 0:
                # 对设备数量进行加密，并进行加倍处理
                license.device_limit = encryption(str(int_device_number * 50))

                # 保存设备数量device_limit的许可到注册表
                # LicenseRegedit.write_regedit(encryption(
                #     str(int_device_number * 50)), 'TrialEmployee')
            else:
                # 对设备数量进行加密，不加倍处理
                license.device_limit = encryption(str(int_device_number))

                # 保存设备数量device_limit的许可到注册表
                # LicenseRegedit.write_regedit(encryption(
                #     str(int_device_number)), 'TrialEmployee')

            # 解析人员employee的许可
            result_employee = zk_license.get_license_info(
                license_number_employee.replace("-", ""))

            license.license_number_employee = license_number_employee

            int_employee_number = int(result_employee['limit_devices'])
            if int(result_employee['minor']) != 0:
                # 对人员数量进行加密，并进行加倍处理
                license.employee_limit = encryption(
                    str(int_employee_number * 500))

                # 保存人员数量employee_limit的许可到注册表
                # LicenseRegedit.write_regedit(encryption(
                #     str(int_employee_number * 500)), 'TrialEmployee')
            else:
                # 对人员数量进行加密，不加倍处理
                license.employee_limit = encryption(str(int_employee_number))

                # 保存人员数量employee_limit的许可到注册表
                # LicenseRegedit.write_regedit(encryption(
                #     str(int_employee_number)), 'TrialEmployee')

            # 解析时间expired_data的许可到数据库
            limite_days = str(result_device['limit_number_of_days'])
            data = date_convert(limite_days)  # 日期转化
            encryption_data = encryption(data)  # 日期进行rsa数据加密
            license.expired_data = encryption_data
            license.save()

            # 保存时间expired_data的许可到注册表
            # LicenseRegedit.write_regedit(encryption_data, 'TrialDate')

            back = {"message": "Your license actived successful!",
                    "type": "offline", "flag": True}

    return render(request, 'license/license_return.html', {'back': back})


@login_required
@permission_required('license.add_license', raise_exception=True)
def active_online(request):
    if request.method != 'POST':
        return render(request, 'license/license_online.html',
                      {'form': OnlineActiveForm()})
    else:
        license_counts = License.objects.all().count()
        if license_counts > 0:
            License.objects.all().delete()  # 删除数据库中已经被修改的license

        license = License()
        form = OnlineActiveForm(request.POST, request.FILES)
        if not form.is_valid():
            return render(request, 'license/license_online.html',
                          {'form': OnlineActiveForm()})
        else:
            license.city = form.cleaned_data.get('city')
            license.contacts = form.cleaned_data.get('contacts')
            license.country = form.cleaned_data.get('country')
            license.industry = form.cleaned_data.get('industry')
            license.address = form.cleaned_data.get('address')
            license.email_address = form.cleaned_data.get('email_address')
            license.company_name = form.cleaned_data.get('company_name')

            license.sn_file = request.FILES.get('sn_file')

            file_type = os.path.splitext(license.sn_file.name)[1]
            if file_type != '.xml':
                back = {"message": "SN file type is Error!",
                        "type": "online", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            size_kb = license.sn_file.size // 1024
            if size_kb > 1024:
                back = {"message": "SN file size is too big!",
                        "type": "offline", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            try:
                content = ElementTree.parse(license.sn_file)
            except Exception:
                back = {"message": "SN file content is Error!",
                        "type": "offline", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            id_element = content.getiterator('id')
            ids = [i.text for i in id_element]
            first_id = ids[0].strip('\n')
            type_id_device = ids[1]
            type_id_employee = ids[2]

            sn_element = content.getiterator('serialNumber')
            serial_number_device = sn_element[0].text
            serial_number_employee = sn_element[1].text

            if int(type_id_device) != 30 or int(type_id_employee) != 35:
                back = {"message": "SN Type Error!",
                        "type": "offline", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            if not serial_number_device and serial_number_employee:
                back = {
                    "message": "Your sn file is wrong, Serial number can not be NULL!",  # noqa E501
                    "type": "offline", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})

            split_sn_device = serial_number_device.replace("-", "")
            split_sn_employee = serial_number_employee.replace("-", "")
            zk_license = ZKLicense()

            upk_device = zk_license.get_upk(split_sn_device)
            upk_employee = zk_license.get_upk(split_sn_employee)

            if not upk_device and upk_employee:
                back = {"message": 'Your sn file is Error!',
                        "type": "offline", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})
            # 组装激活device的xml
            license_info_device = {
                "xmlInfo":
                {
                    "company":
                    {
                        "city": license.city,
                        "country": license.country.name,
                        "industry": license.industry if license.industry else '',  # noqa E501
                        "address": license.address if license.address else '',  # noqa E501
                        "email": license.email_address if license.email_address else '',  # noqa E501
                        "name": license.company_name
                    },
                    "id": int(first_id),
                    "license": [
                        {
                            "serialNumber": serial_number_device,
                            "id": type_id_device,
                            "upk": upk_device
                        }
                    ]
                }
            }
            # 开始进行device在线激活
            url_device = ACTIVITY_URL + str(license_info_device)
            content_device = requests.get(url_device).content
            json_content_device = json.loads(
                str(content_device, encoding='utf-8', errors='ignore'))
            return_code_device = json_content_device['code']

            # 组装激活employee的xml
            license_info_employee = {
                "xmlInfo":
                {
                    "company":
                    {
                        "city": license.city,
                        "country": license.country.name,
                        "industry": license.industry if license.industry else '',  # noqa E501
                        "address": license.address if license.address else '',  # noqa E501
                        "email": license.email_address if license.email_address else '',  # noqa E501
                        "name": license.company_name
                    },
                    "id": int(first_id),
                    "license": [
                        {
                            "serialNumber": serial_number_employee,
                            "id": type_id_employee,
                            "upk": upk_employee
                        }
                    ]
                }
            }
            # 开始进行employee在线激活
            url_employee = ACTIVITY_URL + str(license_info_employee)
            content_employee = requests.get(url_employee).content
            json_content_employee = json.loads(
                str(content_employee, encoding='utf-8', errors='ignore'))
            return_code_employee = json_content_employee['code']

            if return_code_device != 0 or return_code_employee != 0:
                back = {"message": "SN file is Error!",
                        "type": "online", "flag": False}
                return render(request, 'license/license_return.html',
                              {'back': back})
            else:
                license_number_device = json_content_device[
                    'license'][0]['licenseNumber']
                license_number_employee = json_content_employee[
                    'license'][0]['licenseNumber']

                zk_license = ZKLicense()
                # 解析设备device的许可
                result_device = zk_license.get_license_info(
                    license_number_device.replace("-", ""))

                if result_device["major"] == 1:
                    license.pro = 1
                license.license_number_device = license_number_device

                int_device_number = int(result_device['limit_devices'])
                if int(result_device['minor']) != 0:
                    # 对设备数量进行加密，并进行加倍处理
                    license.device_limit = encryption(
                        str(int_device_number * 50))

                    # 保存设备数量device_limit的许可到注册表
                    # LicenseRegedit.write_regedit(encryption(
                    #     str(int_device_number * 50)), 'TrialEmployee')
                else:
                    # 对设备数量进行加密，不加倍处理
                    license.device_limit = encryption(str(int_device_number))

                    # 保存设备数量device_limit的许可到注册表
                    # LicenseRegedit.write_regedit(encryption(
                    #     str(int_device_number)), 'TrialEmployee')

                # 解析人员employee的许可
                result_employee = zk_license.get_license_info(
                    license_number_employee.replace("-", ""))

                license.license_number_employee = license_number_employee

                int_employee_number = int(result_employee['limit_devices'])
                if int(result_employee['minor']) != 0:
                    # 对人员数量进行加密，并进行加倍处理
                    license.employee_limit = encryption(
                        str(int_employee_number * 500))

                    # 保存人员数量employee_limit的许可到注册表
                    # LicenseRegedit.write_regedit(encryption(
                    #     str(int_employee_number * 500)), 'TrialEmployee')
                else:
                    # 对人员数量进行加密，不加倍处理
                    license.employee_limit = encryption(
                        str(int_employee_number))

                    # 保存人员数量employee_limit的许可到注册表
                    # LicenseRegedit.write_regedit(encryption(
                    #     str(int_employee_number)), 'TrialEmployee')

                # 解析时间expired_data的许可到数据库
                limite_days = str(result_device['limit_number_of_days'])
                data = date_convert(limite_days)  # 日期转化
                encryption_data = encryption(data)  # 日期进行rsa数据加密
                license.expired_data = encryption_data
                license.save()

                # 保存时间expired_data的许可到注册表
                # LicenseRegedit.write_regedit(encryption_data, 'TrialDate')

                back = {"message": "Your license actived successful!",
                        "type": "offline", "flag": True}

    return render(request, 'license/license_return.html', {'back': back})
