from __future__ import absolute_import, unicode_literals
from celery import shared_task

import os
import json
import datetime
import logging
import base64
import copy

from mysite.data.models.model_department import Department
from mysite.data.models.model_area import Area
from mysite.data.models.model_transaction import Transaction
from mysite.device.models.model_iclock import Iclock
from mysite.data.models.model_employee import (
    Employee, FingerTemplate, FaceTemplate, BioTemplate)
from mysite.comm.utils import AreaComm, EmployeeComm, DeviceComm
from mysite.comm.get_cmd import UpdateCmd
from mysite.settings import MEDIA_ROOT, COMM_LOG


def str_2_json(info):
    device = {}
    split_info = info.split(',')
    device['push_version'] = split_info[0]
    device['user_count'] = int(split_info[1])
    device['finger_count'] = int(split_info[2])
    device['transaction_count'] = int(split_info[3])
    device['ip_address'] = split_info[4]
    device['finger_version'] = split_info[5]
    device['face_version'] = split_info[6]
    device['face_reg'] = split_info[7]
    device['face_count'] = int(split_info[8])
    device['last_alert_time'] = datetime.datetime.now()\
        .strftime("%Y-%m-%d %H:%M")
    json_info = json.dumps(device)

    return json_info


@shared_task(bind=True, max_retries=3, default_retry_delay=1 * 6)
def save_device(self, sn, info):
    dict_info = json.loads(info)
    device = Iclock.objects.filter(sn=sn)
    if device.exists():
        if 'alias' in dict_info:
            if Iclock.objects.get(sn=sn).alias:
                dict_info.pop('alias')
        try:
            device.update(**dict_info)
            print('update device info success')
        except Exception as e:
            logger = logging.getLogger('collect')
            logger.exception('Exception Logged:%s' % e)
            # raise self.retry(exc=e, countdown=15)
    else:
        dict_info['sn'] = sn
        try:
            Iclock.objects.create(**dict_info)
            print('create device info success')
        except Exception as e:
            logger = logging.getLogger('collect')
            logger.exception('Exception Logged:%s' % e)
            # raise self.retry(exc=e, countdown=15)

    return True


@shared_task(bind=True, max_retries=3, default_retry_delay=1 * 6)
def db_insert(self, data):
    dict_data = json.loads(data)
    iclock = Iclock.objects.get(sn=dict_data['SN'])
    company = iclock.area.company
    for data_field in dict_data['Data']:
        if data_field['data_type'] == 'USER':
            user = data_field.copy()
            user.pop('data_type')
            pin = user['pin']
            company = iclock.area.company
            employee = Employee.objects.filter(
                department__company=company, pin=pin)
            if employee.exists():
                try:
                    employee = employee[0]
                    area_id_iclock = iclock.area.id  # 根据iclock获取对应的area id
                    all_area_ids = [i.id for i in employee.area.all()]
                    if area_id_iclock not in all_area_ids:
                        all_area_ids.append(area_id_iclock)
                        user['area'] = all_area_ids
                    # user.pop('pin')
                    employee.emp_name = user['emp_name']
                    employee.privilege = int(user['privilege'])
                    employee.password = user['password']
                    if user['card'].isdigit() and int(user['card']) == 0:
                        user['card'] = ''
                    employee.card = user['card']
                    employee.area = all_area_ids
                    employee.save()
                    print('update employee success')
                except Exception as e:
                    if COMM_LOG:
                        logger = logging.getLogger('collect')
                        logger.exception('Exception Logged:%s' % e)
                    # raise self.retry(exc=e, countdown=15)
                    pass

                # 组装需要下发的人员信息字典
                cmd_user = {}
                cmd_user['Name'] = user['emp_name']
                cmd_user["Passwd"] = user['password']
                cmd_user['Card'] = user['card']
                cmd_user['Pri'] = user['privilege']

                employee_comm = EmployeeComm()
                area = iclock.area  # 根据iclock获取area
                company = area.company.id  # 根据area获取company
                employee_comm.save_employee(company, pin, cmd_user)
                cmd_user['PIN'] = pin
                # 获取此人员区域下的所有设备,并下发人员信息
                distributeData(iclock, "USERINFO", cmd_user)
            else:
                area = iclock.area  # 根据iclock获取area
                company = area.company  # 根据area获取company
                # 获取此company下的所有departments
                all_department = Department.objects.filter(
                    company=company).order_by('id')  # 根据id排序
                default_department = all_department[0]  # 获取默认的department
                user['department'] = default_department
                if user['card'].isdigit() and int(user['card']) == 0:
                    user['card'] = ''
                try:
                    emp = Employee.objects.create(**user)
                    emp.area = [iclock.area]
                    emp.save()
                    print('save employee success')
                except Exception as e:
                    if COMM_LOG:
                        logger = logging.getLogger('collect')
                        logger.exception('Exception Logged:%s' % e)
                    # raise self.retry(exc=e, countdown=15)
                    pass

                # 保存人员信息到redis中的area中
                area_comm = AreaComm()
                area_comm.save_area_employee(iclock.area.id, pin)

                # 保存人员的area到redis中
                employee_comm = EmployeeComm()
                employee_comm.save_employee_area(
                    company.id, pin, iclock.area.id)

                # 保存人员详细信息到redis中
                other_user = {}
                other_user['Name'] = user['emp_name']
                other_user["Passwd"] = user['password']
                other_user['Card'] = user['card']
                other_user['Pri'] = user['privilege']
                employee_comm.save_employee(company.id, pin, other_user)

                cmd_user = other_user.copy()  # 进行人员字典的深拷贝
                cmd_user['PIN'] = pin
                # 获取此人员区域下的所有设备,并下发人员信息
                distributeData(iclock, "USERINFO", cmd_user)

        elif data_field['data_type'] == 'ATTEN':
            atten = data_field.copy()
            atten.pop('data_type')
            try:
                # pin == 0 is the faild transaction
                if not atten['pin'] == '0':
                    atten['employee'] = Employee.objects.filter(
                        department__company=company, pin=atten['pin'])[0]
                    atten.pop('pin')
                    atten['iclock'] = iclock
                    Transaction.objects.create(**atten)
                    print('save transaction success')
            except Exception as e:
                if COMM_LOG:
                    logger = logging.getLogger('collect')
                    logger.exception('Exception Logged:%s' % e)
                # raise self.retry(exc=e, countdown=6)
                pass

        elif data_field['data_type'] == "FINGER":
            finger = data_field.copy()
            finger.pop('data_type')
            cmd_finger = {}
            cmd_finger['PIN'] = finger['pin']
            cmd_finger['FID'] = finger['template_id']
            cmd_finger['Size'] = finger['size']
            cmd_finger['Valid'] = finger['valid']
            cmd_finger['TMP'] = finger['template']

            try:
                # 当人员和模板同时登记时，人员还未插入到数据库造成错误，使用retry等待人员的插入
                finger['employee'] = Employee.objects.filter(
                    department__company=company, pin=finger['pin'])[0]
            except IndexError as e:
                # 等待并重试插入
                raise self.retry(exc=e, countdown=3)
            except Exception as e:
                # 插入数据错误
                return False

            try:
                finger.pop('pin')
                finger_tmp = FingerTemplate.objects.filter(
                    employee=finger['employee'],
                    template_id=finger['template_id'])
                if finger_tmp.exists():
                    finger_tmp = finger_tmp[0]
                    finger_tmp.employee = finger['employee']
                    finger_tmp.template_id = finger['template_id']
                    finger_tmp.size = finger['size']
                    finger_tmp.valid = finger['valid']
                    finger_tmp.template = finger['template']
                    finger_tmp.save()
                    print('updata finger template success')
                else:
                    FingerTemplate.objects.create(**finger)
                    print('save finger template success')

                distributeData(iclock, "FINGERTMP", cmd_finger)
            except Exception as e:
                print(repr(e))
                if COMM_LOG:
                    logger = logging.getLogger('collect')
                    logger.exception('Exception Logged:%s' % e)
                # raise self.retry(exc=e, countdown=15)
                pass

        elif data_field['data_type'] == "FACE":
            face = data_field.copy()
            face.pop('data_type')
            cmd_face = {}
            cmd_face['PIN'] = face['pin']
            cmd_face['FID'] = face['template_id']
            cmd_face['Size'] = face['size']
            cmd_face['Valid'] = face['valid']
            cmd_face['TMP'] = face['template']

            try:
                # 当人员和模板同时登记时，人员还未插入到数据库造成错误，使用retry等待人员的插入
                face['employee'] = Employee.objects.filter(
                    department__company=company, pin=face['pin'])[0]
            except IndexError as e:
                # 等待并重试插入
                raise self.retry(exc=e, countdown=3)
            except Exception as e:
                # 插入数据错误
                return False

            try:
                face.pop('pin')
                face_tmp = FaceTemplate.objects.filter(
                    employee=face['employee'],
                    template_id=face['template_id'])
                if face_tmp.exists():
                    face_tmp = face_tmp[0]
                    face_tmp.employee = face['employee']
                    face_tmp.template_id = face['template_id']
                    face_tmp.size = face['size']
                    face_tmp.valid = face['valid']
                    face_tmp.template = face['template']
                    face_tmp.save()
                    print('updata face template success')
                else:
                    FaceTemplate.objects.create(**face)
                    print('save face template success')
                distributeData(iclock, "FACE", cmd_face)
            except Exception as e:
                if COMM_LOG:
                    logger = logging.getLogger('collect')
                    logger.exception('Exception Logged:%s' % e)
                pass

        elif data_field['data_type'] == "BIODATA":
            bio = data_field.copy()
            bio.pop('data_type')
            cmd_bio = {}
            cmd_bio['Pin'] = bio['pin']
            cmd_bio['No'] = bio['bio_no']
            cmd_bio['Index'] = bio['bio_index']
            cmd_bio['Valid'] = bio['valid']
            cmd_bio['Duress'] = bio['duress']
            cmd_bio['Type'] = bio['bio_type']
            cmd_bio['MajorVer'] = bio['majorver']
            cmd_bio['MinorVer'] = bio['minorver']
            cmd_bio['Format'] = bio['bio_format']
            cmd_bio['Tmp'] = bio['template']

            try:
                # 当人员和模板同时登记时，人员还未插入到数据库造成错误，使用retry等待人员的插入
                bio['employee'] = Employee.objects.filter(
                    department__company=company, pin=bio['pin'])[0]
            except IndexError as e:
                # 等待并重试插入
                raise self.retry(exc=e, countdown=3)
            except Exception as e:
                # 插入数据错误
                return False

            try:
                bio.pop('pin')
                bio_tmp = BioTemplate.objects.filter(
                    employee=bio['employee'],
                    bio_index=bio['bio_index'],
                    bio_type=bio['bio_type'],
                    majorver=bio['majorver'])
                if bio_tmp.exists():
                    bio_tmp = bio_tmp[0]
                    bio_tmp.employee = bio['employee']
                    bio_tmp.bio_no = bio['bio_no']
                    bio_tmp.bio_index = bio['bio_index']
                    bio_tmp.valid = bio['valid']
                    bio_tmp.duress = bio['duress']
                    bio_tmp.bio_type = bio['bio_type']
                    bio_tmp.majorver = bio['majorver']
                    bio_tmp.minorver = bio['minorver']
                    bio_tmp.bio_format = bio['bio_format']
                    bio_tmp.template = bio['template']
                    bio_tmp.save()
                    print('update biodata template success')
                else:
                    BioTemplate.objects.create(**bio)
                    print('save biodata template success')

                distributeData(iclock, "BIODATA", cmd_bio)
            except Exception as e:
                if COMM_LOG:
                    logger = logging.getLogger('collect')
                    logger.exception('Exception Logged:%s' % e)
                # raise self.retry(exc=e, countdown=15)
                pass

        # ================ save the UserPhote 2018-9-3 ==============
        elif data_field['data_type'] == "USERPIC":
            pic = data_field.copy()
            pic.pop('data_type')
            pic_filename = pic['filename']
            # pic_size = pic['size']
            pic_content = pic['content']
            pic_company = iclock.area.company

            missing_padding = 4 - len(pic_content) % 4
            if missing_padding:
                pic_content += '=' * missing_padding
            pic_content = base64.b64decode(pic_content)  # base64 decode

            path = os.path.join(MEDIA_ROOT, 'user_pic', str(pic_company),
                                pic_filename)
            fn = os.path.split(path)
            if not os.path.exists(fn[0]):
                os.makedirs(fn[0])

            with open(path, "bw") as f:
                f.write(pic_content)

    return True


def distributeData(iclock, cmd_type, data=None):
    employee_comm = EmployeeComm()
    area_comm = AreaComm()
    emp_company = iclock.area.company.id
    pin = data.get('PIN')
    if pin is None:
        pin = data.get('Pin')
    ares_ids = employee_comm.get_employee_all_area(emp_company, pin)
    for area_id in ares_ids:
        area = Area.objects.get(id=area_id)
        # 获取此人员区域下的所有设备,并下发人员人脸模板
        all_devices = area_comm.get_all_area_devices(area.id)  # noqa E530
        if area.id == iclock.area.id:
            all_devices.remove(bytes(iclock.sn, encoding="utf8", errors='ignore'))  # noqa
        for device in all_devices:
            # 将byte类型转化成str
            str_device = str(device, encoding='utf-8', errors='ignore')
            data_tem = copy.deepcopy(data)
            pull_cmd = UpdateCmd(str_device, data_tem, area.company.id, area.id)  # noqa 503
            if cmd_type in ['FINGER', 'FACE', 'BIODATA']:
                if cmd_type == 'FINGER':
                    cmd_type = 'FINGERTMP'
                pull_cmd.update_one_template(data, cmd_type=cmd_type)
            else:
                pull_cmd.update_data(cmd_type=cmd_type)


def save_attphoto(fname, raw_data):
    '''
    保存考勤照片
    '''
    path = os.path.join(MEDIA_ROOT, 'data_store', 'att_pic', fname)
    fn = os.path.split(path)
    if not os.path.exists(fn[0]):
        os.makedirs(fn[0])

    with open(path, "bw") as f:
        f.write(raw_data)


@shared_task(bind=True, max_retries=20, default_retry_delay=1 * 6)
def pull_update_cmd_asyn(self, sn, area):
    area_comm = AreaComm()
    device_comm = DeviceComm()
    employee_comm = EmployeeComm()
    # 判断设备是否已经上传info信息
    if not device_comm.get_device_key(sn, 'SerialNumber')[0]:
        # 如果找不到相应的参数则6s后重试
        retry_info = 'Device info not found! Retry 6s later'
        raise self.retry(exc=retry_info, countdown=6)
    # 将新区域中所有的人员信息下发到该设备中
    all_employees = area_comm.get_all_area_employees(area)  # noqa E501
    company = Area.objects.get(id=area).company.id
    for employee in all_employees:
        str_employee = str(employee, encoding='utf-8', errors='ignore')  # noqa E501
        employee_info = employee_comm.get_employee(company, str_employee)  # noqa E501
        employee_info['PIN'] = str_employee

        pull_cmd = UpdateCmd(sn, employee_info, company, area)
        pull_cmd.update_data('USERINFO')
        pull_cmd.update_data('FINGERTMP')
        pull_cmd.update_data('FACE')
        pull_cmd.update_data('BIODATA')
