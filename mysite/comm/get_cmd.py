import time

from mysite.data.models.model_employee import (
    Employee, FaceTemplate, FingerTemplate, BioTemplate)
from mysite.utils.userid_format import pin_2_userid
from mysite.data.models.model_area import Area
from mysite.comm.utils import CmdComm, DeviceComm, AreaComm, EmployeeComm


class UpdateCmd():

    def __init__(self, sn, dict_employee, company, area):
        self.sn = sn
        self.dict_employee = dict_employee

        self.num_card = dict_employee.get('Card')
        # 将十进制数字card转化成设备能识别的16进制card， 否则下发设备后不识别
        if not self.num_card or not self.num_card.strip():
            self.dict_employee['Card'] = '[0000000000]'
        else:
            try:
                card = int(str(self.num_card))
                card_1 = card & 255
                card_2 = card >> 8 & 255
                card_3 = card >> 16 & 255
                card_4 = card >> 24 & 255
                card_5 = card >> 32 & 255
                key = '[%02X%02X%02X%02X%02X]'
                final_card = key % (card_1, card_2, card_3, card_4, card_5)

                self.dict_employee['Card'] = final_card
            except Exception:
                self.dict_employee['Card'] = '[0000000000]'

        if self.dict_employee['Card'] == '[0000000000]':
            self.dict_employee['Card'] = ''

        self.company = company
        self.area = area
        self.space_type = " "
        self.table_type = "\t"
        self.cur_time = str(int(time.time() * 10000000))[1:17]
        self.cmd = CmdComm()

    def update_data(self, cmd_type):
        if cmd_type == "USERINFO":
            self.update_userinfo(cmd_type)
        else:
            self.update_template(cmd_type)

    def get_content(self, cmd_type, dict_data):
        base_content = "C:{0}:DATA{1}UPDATE{2}{3}{4}".format(
            self.cur_time, self.space_type,
            self.space_type, cmd_type, self.space_type)

        content_list = []
        for k, v in dict_data.items():
            content = "{0}={1}".format(k, v)
            content_list.append(content)

        other_content = self.table_type.join(content_list)
        convert_content = other_content + '\n'
        final_content = base_content + convert_content

        return final_content

    def update_userinfo(self, cmd_type):
        self.cur_time = str(int(time.time() * 10000000))[1:17]
        self.cur_time = self.cmd.check_flag_exist(self.sn, self.cur_time)
        final_content = self.get_content(cmd_type, self.dict_employee)
        flag = '{}'.format(self.cur_time)
        # self.conn.hmset('%s' % self.sn, {flag: final_content})
        self.cmd.save_cmd(self.sn, final_content, flag)

        return True

    def update_template(self, cmd_type):
        sn = self.sn
        area_object = Area.objects.get(id=self.area)
        device_comm = DeviceComm()
        finger_fun_on = device_comm.get_device_key(sn, 'FingerFunOn')[0]
        if finger_fun_on:
            finger_fun_on = str(finger_fun_on, encoding='utf-8', errors='ignore')  # noqa E503
        face_fun_on = device_comm.get_device_key(sn, 'FaceFunOn')[0]
        if face_fun_on:
            face_fun_on = str(face_fun_on, encoding='utf-8', errors='ignore')
        face_version = device_comm.get_device_key(sn, 'FaceVersion')[0]
        if face_version:
            face_version = str(face_version, encoding='utf-8', errors='ignore')
        pv_fun_on = device_comm.get_device_key(sn, 'PvFunOn')[0]
        if pv_fun_on:
            pv_fun_on = str(pv_fun_on, encoding='utf-8', errors='ignore')

        if cmd_type == "BIODATA":
            dict_template = {}
            # 判断缓存中数据跟数据库中的数据是否一致
            employee = Employee.objects.filter(
                area=area_object, pin=self.dict_employee["PIN"])
            # 如果不一致【缓存中有employee，数据库中没有employee】
            if not employee.exists():
                area_comm = AreaComm()
                # 删除缓存area中employee的数据
                area_comm.del_area_employee(
                    self.area, self.dict_employee["PIN"])
                # 删除缓存employee中area数据和employee info数据
                employee_comm = EmployeeComm()
                employee_comm.del_employee_area(
                    self.company, self.dict_employee["PIN"], self.area)
                employee_comm.del_employee(
                    self.company, self.dict_employee["PIN"])
            else:
                employee_id = Employee.objects.get(
                    area=area_object, pin=self.dict_employee['PIN']).id
                all_templates = BioTemplate.objects.filter(
                    employee=employee_id)

                palm_temp = BioTemplate.objects.none()
                face_temp = BioTemplate.objects.none()
                # 掌纹模板
                if pv_fun_on == '1':
                    palm_temp = all_templates.filter(bio_type='8')
                # 9.0面部模板
                if face_fun_on == '1' and face_version == '9':
                    face_temp = all_templates.filter(bio_type='2')
                all_templates = palm_temp | face_temp

                for template in all_templates:
                    dict_template['Pin'] = pin_2_userid(template.employee.pin)
                    dict_template['No'] = str(template.bio_no)
                    dict_template['Index'] = str(template.bio_index)
                    dict_template['Valid'] = str(template.valid)
                    dict_template['Duress'] = str(template.duress)
                    dict_template['Type'] = str(template.bio_type)
                    dict_template['MajorVer'] = template.majorver
                    dict_template['MinorVer'] = template.minorver
                    dict_template['Format'] = str(template.bio_format)
                    # 此处Tmp为首字母大写 与fp和face不同
                    dict_template['Tmp'] = template.template

                    self.cur_time = str(int(time.time() * 10000000))[1:17]
                    self.cur_time = self.cmd.check_flag_exist(self.sn, self.cur_time)  # noqa
                    final_content = self.get_content(cmd_type, dict_template)
                    flag = '{}'.format(self.cur_time)
                    # self.conn.hmset('%s' % self.sn, {flag: final_content})
                    self.cmd.save_cmd(self.sn, final_content, flag)
        else:
            dict_template = {}
            # 判断缓存中数据跟数据库中的数据是否一致
            employee = Employee.objects.filter(
                area=area_object, pin=self.dict_employee["PIN"])
            # 如果不一致【缓存中有employee，数据库中没有employee】
            if not employee.exists():
                area_comm = AreaComm()
                # 删除缓存area中employee的数据
                area_comm.del_area_employee(
                    self.area, self.dict_employee["PIN"])
                # 删除缓存employee中area数据和employee info数据
                employee_comm = EmployeeComm()
                employee_comm.del_employee_area(
                    self.company, self.dict_employee["PIN"], self.area)
                employee_comm.del_employee(
                    self.company, self.dict_employee["PIN"])
            else:
                employee_id = Employee.objects.get(
                    area=area_object, pin=self.dict_employee['PIN']).id
                if cmd_type == "FINGERTMP":
                    if finger_fun_on == '1':
                        all_templates = FingerTemplate.objects.filter(
                            employee=employee_id)
                    else:
                        all_templates = FingerTemplate.objects.none()
                else:
                    if face_fun_on == '1' and face_version == '7':
                        all_templates = FaceTemplate.objects.filter(
                            employee=employee_id)
                    else:
                        all_templates = FaceTemplate.objects.none()

                for template in all_templates:
                    dict_template = {}
                    dict_template['PIN'] = pin_2_userid(template.employee.pin)
                    dict_template['FID'] = str(template.template_id)
                    dict_template['Size'] = str(len(template.template))
                    dict_template['Valid'] = str(template.valid)
                    dict_template['TMP'] = template.template

                    self.cur_time = str(int(time.time() * 10000000))[1:17]
                    self.cur_time = self.cmd.check_flag_exist(self.sn, self.cur_time)  # noqa
                    final_content = self.get_content(cmd_type, dict_template)
                    flag = '{}'.format(self.cur_time)
                    # self.conn.hmset('%s' % self.sn, {flag: final_content})
                    self.cmd.save_cmd(self.sn, final_content, flag)

        return True

    # 如果更新的是face或bio数据时
    # 一个人的特征需要多条template数据，故增加下发一条模板记录功能
    def update_one_template(self, dict_template, cmd_type):
        sn = self.sn
        device_comm = DeviceComm()
        right_flag = False
        if cmd_type == 'FINGERTMP':
            finger_fun_on = device_comm.get_device_key(sn, 'FingerFunOn')[0]
            if finger_fun_on:
                finger_fun_on = str(finger_fun_on, encoding='utf-8', errors='ignore')  # noqa E503
                if finger_fun_on == '1':
                    right_flag = True

        elif cmd_type == 'FACE':
            face_fun_on = device_comm.get_device_key(sn, 'FaceFunOn')[0]
            if face_fun_on:
                face_fun_on = str(face_fun_on, encoding='utf-8', errors='ignore')  # noqa E503
            face_version = device_comm.get_device_key(sn, 'FaceVersion')[0]
            if face_version:
                face_version = str(face_version, encoding='utf-8', errors='ignore')  # noqa E503
            if face_fun_on == '1' and face_version == '7':
                right_flag = True

        elif cmd_type == 'BIODATA':
            bio_type = dict_template.get('Type')
            if bio_type == '8':
                pv_fun_on = device_comm.get_device_key(sn, 'PvFunOn')[0]
                if pv_fun_on:
                    pv_fun_on = str(pv_fun_on, encoding='utf-8', errors='ignore')  # noqa E503
                    if pv_fun_on == '1':
                        right_flag = True
            elif bio_type == '2':
                face_fun_on = device_comm.get_device_key(sn, 'FaceFunOn')[0]
                if face_fun_on:
                    face_fun_on = str(face_fun_on, encoding='utf-8', errors='ignore')  # noqa E503
                face_version = device_comm.get_device_key(sn, 'FaceVersion')[0]
                if face_version:
                    face_version = str(face_version, encoding='utf-8', errors='ignore')  # noqa E503

                if face_fun_on == '1' and face_version == '9':
                    right_flag = True
        else:
            pass

        if right_flag:
            self.cur_time = str(int(time.time() * 10000000))[1:17]
            self.cur_time = self.cmd.check_flag_exist(self.sn, self.cur_time)
            final_content = self.get_content(cmd_type, dict_template)
            flag = '{}'.format(self.cur_time)
            # self.conn.hmset('%s' % self.sn, {flag: final_content})
            self.cmd.save_cmd(sn, final_content, flag)

    def updata_userpic(self):
        pass


class DeleteCmd():

    def __init__(self, sn, emp_pin):
        self.sn = sn
        self.emp_pin = emp_pin
        # self.conn = get_redis_client()
        self.cmd = CmdComm()
        self.space_type = " "
        self.table_type = "\t"
        self.cur_time = str(int(time.time() * 10000000))[1:17]

    def delete_fingertemp(self, cmd_type):

        all_templates = FingerTemplate.objects.filter(
            employee=self.emp_pin)

        for template in all_templates:
            finger_id = template.template_id

            self.cur_time = str(int(time.time() * 10000000))[1:17]
            self.cur_time = self.cmd.check_flag_exist(self.sn, self.cur_time)
            base_content = "C:{0}:DATA{1}DELETE{2}{3}{4}".format(
                self.cur_time, self.space_type,
                self.space_type, cmd_type, self.space_type)

            other_content = "PIN={0}{1}PID={2}".format(
                pin_2_userid(self.emp_pin), self.table_type, finger_id)
            convert_content = other_content + '\n'
            final_content = base_content + convert_content

            flag = '{}'.format(self.cur_time)
            # self.conn.hmset('%s' % self.sn, {flag: final_content})
            self.cmd.save_cmd(self.sn, final_content, flag)

        return True

    def other_delete(self, cmd_type):
        self.cur_time = str(int(time.time() * 10000000))[1:17]
        self.cur_time = self.cmd.check_flag_exist(self.sn, self.cur_time)

        base_content = "C:{0}:DATA{1}DELETE{2}{3}{4}".format(
            self.cur_time, self.space_type,
            self.space_type, cmd_type, self.space_type)
        other_content = "PIN={}".format(pin_2_userid(self.emp_pin))

        convert_content = other_content + '\n'
        final_content = base_content + convert_content

        flag = '{}'.format(self.cur_time)
        # self.conn.hmset('%s' % self.sn, {flag: final_content})
        self.cmd.save_cmd(self.sn, final_content, flag)

        return True


class QueryCmd():

    def __init__(self, sn):
        self.sn = sn
        # self.conn = get_redis_client()
        self.cmd = CmdComm()
        self.space_type = " "
        self.table_type = "\t"
        self.cur_time = str(int(time.time() * 10000000))[1:17]

    def query_userinfo(self, cmd_type, emp_pin):
        base_content = "C:{0}:DATA{1}QUERY{2}{3}{4}".format(
            self.cur_time, self.space_type,
            self.space_type, cmd_type, self.space_type)
        other_content = "PIN={}".format(pin_2_userid(emp_pin))

        convert_content = other_content + '\n'
        final_content = base_content + convert_content

        flag = '{}'.format(self.cur_time)
        # self.conn.hmset('%s' % self.sn, {flag: final_content})
        self.cmd.save_cmd(self.sn, final_content, flag)

        return True

    def query_fingertemp(self, cmd_type, emp_pin):

        all_templates = FingerTemplate.objects.filter(
            employee=emp_pin)

        for template in all_templates:

            self.cur_time = str(int(time.time() * 10000000))[1:17]
            self.cur_time = self.cmd.check_flag_exist(self.sn, self.cur_time)

            finger_id = template.template_id
            base_content = "C:{0}:DATA{1}QUERY{2}{3}{4}".format(
                self.cur_time, self.space_type,
                self.space_type, cmd_type, self.space_type)

            other_content = "PIN={0}{1}FingerID={2}".format(
                pin_2_userid(emp_pin), self.table_type, finger_id)

            convert_content = other_content + '\n'
            final_content = base_content + convert_content

            flag = '{}'.format(self.cur_time)
            # self.conn.hmset('%s' % self.sn, {flag: final_content})
            self.cmd.save_cmd(self.sn, final_content, flag)

        return True

    def other_query(self, cmd_type, start_time, end_time):
        self.cur_time = str(int(time.time() * 10000000))[1:17]
        self.cur_time = self.cmd.check_flag_exist(self.sn, self.cur_time)

        base_content = "C:{0}:DATA{1}QUERY{2}{3}{4}".format(
            self.cur_time, self.space_type,
            self.space_type, cmd_type, self.space_type)
        other_content = "StartTime={}{}EndTime={}".format(
            start_time, self.table_type, end_time)

        convert_content = other_content + '\n'
        final_content = base_content + convert_content

        flag = '{}'.format(self.cur_time)
        # self.conn.hmset('%s' % self.sn, {flag: final_content})
        self.cmd.save_cmd(self.sn, final_content, flag)

        return True


def clear_cmd(sn, cmd_type):
    cmd = CmdComm()
    space_type = " "
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    content = "C:{0}:CLEAR{1}{2}".format(
        cur_time, space_type, cmd_type)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    # conn = get_redis_client()
    # conn.hmset('%s' % sn, {flag: content})
    cmd.save_cmd(sn, convert_content, flag)

    return True


def check_cmd(sn, cmd_type):
    cmd = CmdComm()
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    content = "C:{0}:{1}".format(cur_time, cmd_type)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    # conn = get_redis_client()
    # conn.hmset('%s' % sn, {flag: content})
    cmd.save_cmd(sn, convert_content, flag)

    return True


def check_cmd_verify(sn, cmd_type, start_time, end_time):
    cmd = CmdComm()
    space_type = " "
    table_type = "\t"
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    content = "C:{0}:{1}SUM{2}ATTLOG{3}StartTime={4}{5}EndTime={6}"\
        .format(cur_time, cmd_type, space_type, space_type,
                start_time, table_type, end_time)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    # conn = get_redis_client()
    # conn.hmset('%s' % sn, {flag: content})
    cmd.save_cmd(sn, convert_content, flag)

    return True


def reboot_cmd(sn):
    cmd = CmdComm()
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    content = "C:{}:REBOOT".format(cur_time)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    # conn = get_redis_client()
    # conn.hmset('%s' % sn, {flag: content})
    cmd.save_cmd(sn, convert_content, flag)

    return True


def get_info_cmd(sn):
    cmd = CmdComm()
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    content = "C:{}:INFO".format(cur_time)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    # conn = get_redis_client()
    # conn.hmset('%s' % sn, {flag: content})
    cmd.save_cmd(sn, convert_content, flag)

    return True


def reload_options_cmd(sn):
    cmd = CmdComm()
    space_type = " "
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    content = "C:{}:RELOAD{}OPTIONS".format(cur_time, space_type)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    # conn = get_redis_client()
    # conn.hmset('%s' % sn, {flag: content})
    cmd.save_cmd(sn, convert_content, flag)

    return True


# def set_options_cmd_stamp(sn):
#     cmd = CmdComm()
#     space_type = " "
#     cur_time = str(int(time.time() * 10000000))[1:17]
#     cur_time = cmd.check_flag_exist(sn, cur_time)
#     content = "C:{0}:SET{1}OPTION{2}OPERLOGStamp={3}".format(
#         cur_time, space_type, space_type, 0)

#     flag = '{}'.format(cur_time)

#     conn = get_redis_client()
#     conn.hmset('%s' % sn, {flag: content})
#     cmd.save_cmd(sn, content, flag)

#     return True


def set_options_cmd_timezone(sn, timezone):
    cmd = CmdComm()
    space_type = " "
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    content = "C:{0}:SET{1}OPTION{2}TimeZone={3}".format(
        cur_time, space_type, space_type, timezone)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    # conn = get_redis_client()
    # conn.set(sn + '_timezone', timezone)
    # conn.hmset('%s' % sn, {flag: content})
    cmd.save_cmd(sn, convert_content, flag)

    return True


def updata_workcode(sn, work_code, work_name):
    '''
    从软件中下发workcode到设备
    '''
    cmd = CmdComm()
    space_type = " "
    table_type = "\t"
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    # 下发workcode命令可能还有大小写问题 现在没有标准 测试可行
    content = "C:{0}:DATA{1}UPDATE{2}WORKCODE{3}CODE={4}{5}NAME={6}".format(
        cur_time, space_type, space_type,
        space_type, work_code, table_type, work_name
    )

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    cmd.save_cmd(sn, convert_content, flag)

    return True


def delete_workcode(sn, work_code):
    '''
    从软件中删除workcode到设备
    '''
    cmd = CmdComm()
    space_type = " "
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    content = "C:{0}:DATA{1}DELETE{2}WORKCODE{3}CODE={4}".format(
        cur_time, space_type, space_type, space_type, work_code
    )

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    cmd.save_cmd(sn, convert_content, flag)

    return True


def pull_advpic(sn, url, file_name):
    '''
    从软件下发广告照片到设备
    '''
    cmd = CmdComm()
    space_type = " "
    table_type = "\t"
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    file_path = '/mnt/mtdblock/commonres/adpic/{}'.format(file_name)
    content = "C:{0}:PutFile{1}{2}{3}{4}".format(
        cur_time, space_type, url, table_type, file_path)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    cmd.save_cmd(sn, convert_content, flag)

    return True


def pull_restoredy(sn, url):
    cmd = CmdComm()
    space_type = " "
    table_type = "\t"
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    file_path = '/mnt/mtdblock/'
    content = "C:{0}:PutFile{1}{2}{3}{4}".format(
        cur_time, space_type, url, table_type, file_path)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    cmd.save_cmd(sn, convert_content, flag)

    return True


def set_client_option(sn, key, value):  # 设置客户端选项，可用来决定是否开启设备打包上传数据等操作
    cmd = CmdComm()
    cur_time = str(int(time.time() * 10000000))[1:17]
    cur_time = cmd.check_flag_exist(sn, cur_time)
    content = "C:{0}:SET OPTION {1}={2}".format(cur_time, key, value)

    convert_content = content + '\n'

    flag = '{}'.format(cur_time)

    # conn = get_redis_client()
    # conn.hmset('%s' % sn, {flag: content})
    cmd.save_cmd(sn, convert_content, flag)

    return True
