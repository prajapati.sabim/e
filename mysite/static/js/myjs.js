//获取url的参数
function getParam(paramKey){
    //获取当前URL
    var url = location.href;
    //获取要取得的get参数位置
    var get = url.indexOf(paramKey +"=");
    if(get == -1){
        return "";
    }
    //截取字符串
    var getParamStr = url.slice(paramKey.length + get + 1);
    //判断截取后的字符串是否还有其他get参数
    var nextparam = getParamStr.indexOf("&");
    if(nextparam != -1){
        getParamStr = getParamStr.slice(0, nextparam);
    }
    return decodeURIComponent(getParamStr);
}

//添加url参数
function addParam(url,paramKey,paramVal){
    var andStr = "?";
    var beforeparam = url.indexOf("?");
    if(beforeparam != -1){
        andStr = "&";
    }
    return url + andStr + paramKey + "="+ encodeURIComponent(paramVal);
}

//删除url参数
function delParam(url,paramKey){
    var urlParam = url.substr(url.indexOf("?")+1);
    var beforeUrl = url.substr(0,url.indexOf("?"));
    var nextUrl = "";

    var arr = new Array();
    if(urlParam!=""){
        var urlParamArr = urlParam.split("&");

        for(var i=0;i<urlParamArr.length;i++){
            var paramArr = urlParamArr[i].split("=");
            if(paramArr[0]!=paramKey){
                arr.push(urlParamArr[i]);
            }
        }
    }

    if(arr.length>0){
        nextUrl = "?"+arr.join("&");
    }
    url = beforeUrl+nextUrl;
    return url;
}

// 设置弹窗属性
function pop_windous(width, height, title)
{
    $("#mask").fadeIn("fast");
    $("#pop_window").css({ "width":width, "height":height});
    $("#pop_window").css({ "margin-left": -width/2, "margin-top": -height/2});
    $("#pop_window_title").children('h').text(title)
    $("#pop_window").fadeIn("fast");
}
//  关闭弹窗
$(function ($) {
    $(".close_btn").hover(function () { $(this).css({ color: 'black' }) }, function () { $(this).css({ color: '#666' }) }).on('click', function () {
    $("#pop_window").fadeOut("fast");
    $("#mask").css({ display: 'none' });
  });

});