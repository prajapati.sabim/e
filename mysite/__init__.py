from __future__ import absolute_import, unicode_literals

import pymysql
# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from mysite.celery import app as celery_app
from mysite.utils.version import get_version

__all__ = ['celery_app']

pymysql.install_as_MySQLdb()

VERSION = (6, 4, 4, 'final', 1298)  # set wdms version

__version__ = get_version(VERSION)
