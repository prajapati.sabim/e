$(function(){
    //移到右边
    $('#add').click(function() {
    //获取选中的选项，删除并追加给对方
        $('#choose option:selected').appendTo('#chosen');
    });
    //移到左边
    $('#remove').click(function() {
        $('#chosen option:selected').appendTo('#choose');
    });
    //全部移到右边
    // $('#add_all').click(function() {
    //     //获取全部的选项,删除并追加给对方
    //     $('#choose option').appendTo('#chosen');
    // });
    // //全部移到左边
    // $('#remove_all').click(function() {
    //     $('#chosen option').appendTo('#choose');
    // });
    //双击选项
    $('#choose').dblclick(function(){ //绑定双击事件
        //获取全部的选项,删除并追加给对方
        $("option:selected",this).appendTo('#chosen'); //追加给对方
    });
    //双击选项
    $('#chosen').dblclick(function(){
       $("option:selected",this).appendTo('#choose');
    });
});