import os
from distutils.core import setup

from Cython.Build import cythonize


def get_sourcefiles(path):
    sourcefiles = []
    otherfiles = []

    for root, dirs, files in os.walk(path):
        for file in files:
            file_split = file.split('.')
            file_type = file_split[-1]
            file_startswith = file_split[0].startswith('0')

            if file != '__init__.py' and file_type == 'py' and not file_startswith:  # noqa E501
                absolute_path_file = os.path.join(root, file)
                sourcefiles.append(absolute_path_file)
            else:
                absolute_path_file = os.path.join(root, file)
                otherfiles.append(absolute_path_file)

    return sourcefiles


setup(
    name='py_2_pyd',
    version='0.0.1',
    author='Robin.Chen',
    description='build files from py to pyd',
    ext_modules=cythonize(get_sourcefiles('../mysite/'))
    # ext_modules=cythonize(['test.py'])
)
