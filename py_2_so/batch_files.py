import os
import shutil


def split_filename(filename):
    f_split = filename.split('.')
    f_split.pop(1)

    new_filename = ('.').join(f_split)

    return new_filename


def change_filename():
    path = os.path.abspath('./mysite/')

    for root, dirs, files in os.walk(path):

        for f in files:
            abs_filepath = os.path.join(root, f)
            new_filepath = os.path.join(root, split_filename(f))
            os.rename(abs_filepath, new_filepath)
            print('#######rename %s to %s' % (abs_filepath, new_filepath))

    return True


def change_filepath(filepath):
    middle_file = 'py_2_so'

    file_split = filepath.split('/')
    index = file_split.index('mysite')
    file_split.insert(index, middle_file)

    new_filepath = ('/').join(file_split)

    return new_filepath


def get_files():
    path = os.path.abspath('../mysite/')

    for root, dirs, files in os.walk(path):
        new_root = change_filepath(root)

        for d in dirs:
            new_dir_path = os.path.join(new_root, d)
            if not os.path.exists(new_dir_path):
                os.makedirs(new_dir_path)

        for f in files:
            f_split = f.split('.')
            f_type = f_split[-1]
            f_startswith = f_split[0].startswith('000')

            if f == '__init__.py' or f_type != 'py' or f_startswith:
                abs_filepath = os.path.join(root, f)

                shutil.copy(abs_filepath, new_root)
                print('---->>>copy %s to %s' % (abs_filepath, new_root))
                print('***copy file success***')

                if f_type in ['pyc', 'c']:
                    os.remove(abs_filepath)
                    os.remove(os.path.join(new_root, f))
                    print('**********delete file success****************')

    return True


def copy_wsgi():
    old_wsgi = os.path.abspath('../mysite/wsgi.py')
    new_wsgi = os.path.abspath('./mysite/')
    shutil.copy(old_wsgi, new_wsgi)


if __name__ == '__main__':
    change_filename()
    get_files()
    copy_wsgi()
