#!/user/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

echo "this is docker-compose sevicie status:"
docker-compose ps
echo "--------------------------------------------------"
echo "this is docker-compose service detail"
docker-compose top
